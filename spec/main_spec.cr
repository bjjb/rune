require "spec"

describe "running main.cr" do
  status = Process.run("crystal", %w(run src/main.cr -- -h))
  it "returns successfully" do
    status.success?.should be_true
  end
end
