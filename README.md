Rún
===

> ruːn

Rún (pronounced like "rune" in English), helps you maintain your [PGP][1]
keys.

The problem
-----------

PGP is great, and everyone should use it for all their communication, git
commits, etc. However, setting up PGP to work consistently on multiple
machines is not so easy, even when you're just trying to manage your own
identities. Allowing _anyone_ or _anything_ access to your private keys is an
absolute no-no (unless that key itself is encrypted safely, so you see the
chicken-and-egg scenario).

The solution
------------

Building on several useful implementations of keychains, vaults, etc, I
propose a command-line client which will consistently provide a subset of the
commands available to [GPG2][2], but which will work in any POSIX environment.
This tool should transparently store the keys in a secure keychain of the
user's choosing. On MacOS this could be the OSX Keychain, for example, whereas
on Linux, it could be `pass`, or the desktop environment's secret store. In the
former case, Apple automatically provides synchronization across devices. In
the latter, we need to provide a mechanism which does the same, probably based
on another of the user's online credentials (Google, Dropbox, Box, Microsoft,
Facebook, Twitter, etc). Export needs to be completely opaque at rest and on
the wire. In order to potentially monetise it, we could also provide our own
secure storage, for objects which only the end-user can decrypt, which can then
be imported (using the tool) to provide them with a consistent set of
identities and public-keys everywhere they log in.

The motivation
--------------

I recently started using PGP a lot more to encrypt sensitive emails and to
sign Git commits, and I am encouraging all my colleagues to do the same. It's
also been great for securely generating server credentials for team members
and distributing them safely. The ability to encrypt something (such as a
server configuration) which can then be decrypted by a controllable set of
users is also super cool. I just wish the tooling were better.

Also, I want a good project with which to improve my Rust skills.
Unfortunately, there's already a crate called [rune][3], so I decided to pivot
to [Crystal][4].

To Do
-----

- [ ] hello world template using both library and binary
- [ ] basic `list` subcommand
- [ ] basic `doctor` subcommand
- [ ] basic `export` subcommand
- [ ] configuration library
- [ ] heuristic `configure` command
- [ ] `server` command (hello-world is fine)
- [ ] Dockerfile
- [ ] build pipeline
- [ ] database for server
- [ ] authentication module for server
- [ ] vault encryption

[1]: https://en.wikipedia.org/wiki/Pretty_Good_Privacy
[2]: https://gnupg.org/
[3]: https://crates.io/crates/rune
[4]: https://crystal-lang.org
