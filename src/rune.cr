require "openssl/cipher"

module Rune
  VERSION = "0.1.0"

  def self.encrypt(data, key, iv)
    cipher = OpenSSL::Cipher.new("aes-256-cbc")
    cipher.encrypt
    cipher.key = key
    cipher.iv = iv

    io = IO::Memory.new
    io.write(cipher.update(data))
    io.write(cipher.final)
    io.rewind

    io.to_slice
  end

  def self.decrypt(data, key, iv)
    cipher = OpenSSL::Cipher.new("aes-256-cbc")
    cipher.encrypt
    cipher.key = key
    cipher.iv = iv

    io = IO::Memory.new
    io.write(cipher.update(data))
    io.write(cipher.final)
    io.rewind

    io.gets_to_end
  end
end
