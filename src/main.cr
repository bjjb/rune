require "option_parser"
require "./rune"

def fail(message, code = 1)
  STDERR.puts(message)
  exit(code)
end

OptionParser.parse do |p|
  key = "foo" # TODO
  iv = "bar"  # TODO

  p.banner = "Usage: rune [command] [args...]"
  p.unknown_args do |args, cmd|
    fail(p) if args.empty?
    args.each { |arg| puts Rune.decrypt(arg, key, iv) }
  end
  p.separator("Options:")
  p.on("-h", "--help", "print help and exit") { p.unknown_args { puts p } }
  p.on("-V", "--version", "print the version and exit") do
    p.unknown_args { puts "rune v#{Rune::VERSION}" }
  end
end
